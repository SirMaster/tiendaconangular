import { Component, OnInit } from '@angular/core';
import { CatalogoProducto } from './../models/catalogo-producto.model';

@Component({
  selector: 'app-listado-productos',
  templateUrl: './listado-productos.component.html',
  styleUrls: ['./listado-productos.component.css']
})
export class ListadoProductosComponent implements OnInit {
    producto: CatalogoProducto[];
    constructor() { 
      this.producto = [];
  }

  ngOnInit(): void {
  }

  guardar( nombre:string, descripcion:string, url:string ):boolean{
    this.producto.push (new CatalogoProducto (nombre, descripcion, url) );
    return false;
  }

  elegido(p) {
    this.producto.forEach(function (x) {x.setSelected(false); });
    p.setSelected(true);
  }

}

import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { CatalogoProducto } from './../models/catalogo-producto.model';

@Component({
  selector: 'app-catalogo-productos',
  templateUrl: './catalogo-productos.component.html',
  styleUrls: ['./catalogo-productos.component.css']
})
export class CatalogoProductosComponent implements OnInit {
@Input() producto: CatalogoProducto;
@Input() posicion: number;
@HostBinding('attr.class') cssClass= 'col-md-4';
@Output() clicked : EventEmitter<CatalogoProducto>;

  constructor() { 
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir() {
    this.clicked.emit(this.producto);
    return false;
  }

}

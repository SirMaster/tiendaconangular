import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CatalogoProductosComponent } from './catalogo-productos/catalogo-productos.component';
import { ListadoProductosComponent } from './listado-productos/listado-productos.component';
import { DetalleProductoComponent } from './detalle-producto/detalle-producto.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: ListadoProductosComponent},
  {path: 'detalle', component: DetalleProductoComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    CatalogoProductosComponent,
    ListadoProductosComponent,
    DetalleProductoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

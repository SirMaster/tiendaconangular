export class CatalogoProducto{
    private selected: boolean;
    public estado: string [];
    constructor(public nombre: string, public descripcion: string, public url: string){
        this.estado = ['Nuevo', 'Usado'];
    }
    isSelected():boolean {
        return this.selected;
    }
    setSelected(s: boolean) {
        this.selected = s;
    }
}